import { Router } from 'express';
import { EventService } from "../services/event";
import { EventController } from "../controllers/event";

// Attends un EventRepository en paramètre, et instancie l'EventService
// puis l'EventController avant de bind les routes et de retourner le routeur
export default function eventRoutesFactory( eventService: EventService ) {
    const eventController = new EventController(eventService);
    
    const router = Router();
    router.post("/", (req, res) => eventController.addEvent(req, res));

    return router;
}
