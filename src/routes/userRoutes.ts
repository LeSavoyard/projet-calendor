import { Router } from 'express';
import { UserService } from '../services/user';
import { UserController } from '../controllers/user';


export default function userRoutesFactory( userService: UserService ) {
    const router = Router();
    const userController = new UserController(userService);
    
    router.post("/", (req, res) => userController.addUser(req, res));
    router.get("/", (req, res) => userController.findAll(req, res));
    router.get("/", (req, res) => userController.findById(req, res));
   
    return router;
}