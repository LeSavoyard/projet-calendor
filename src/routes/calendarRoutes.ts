import { Router } from 'express';
import { CalendarController }  from '../controllers/calendar';
import {CalendarService} from '../services/calendar';


// Merci Lucien pour ce Moment.js
export default function calendarRoutesFactory( calendarService: CalendarService ) {
    const calendarController = new CalendarController(calendarService);
    
    const router = Router();
    
    router.get("/", calendarController.fetchAll.bind(calendarController));
    router.get("/:calendarId", (req, res) => calendarController.getCalendar(req, res));
    
    router.post("/", (req, res) => calendarController.createCalendar(req, res));
    
    router.post("/:calendarId/addUser", (req, res) => calendarController.addUserToCalendar(req, res));
    router.put("/:calendarId/addUser", (req, res) => calendarController.addUserToCalendar(req, res));

    return router;
}