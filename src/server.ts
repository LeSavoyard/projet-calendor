import express from 'express';
import bodyParser from "body-parser";
import { PrismaClient } from '@prisma/client';

import eventRoutesFactory from "./routes/event";
import calendarRoutesFactory from "./routes/calendarRoutes";
import userRoutesFactory from "./routes/userRoutes";
import { CalendarService } from './services/calendar';
import { UserService } from './services/user';
import { EventService } from './services/event';

export const app = express();

app.use(bodyParser.json());


export const prisma = new PrismaClient();

// Puisqu'on fait de l'Injection de Dépendences, on doit en premier instancier un PrismaClient,
// puis instancier nos Services en leur passant leur dépendences (Prisma = notre "repository")
const calendarService = new CalendarService(prisma.calendar);
const userService = new UserService(prisma.user);
const eventService = new EventService(prisma.event, userService);

// On utilise des "factory" pour pouvoir les laisser gérer l'instanciation
// du controlleur (et leur passer les Services)
app.use('/calendar', calendarRoutesFactory(calendarService));
app.use("/users", userRoutesFactory(userService));
app.use('/event', eventRoutesFactory(eventService));
