import { PrismaClient } from '@prisma/client'

export type CreateCalendarDTO = {
    userId: number;
}

export type AddUserToCalendarDTO = {
    calendarId: number;
    userId: number;
}
export type GetCalendarDTO = {
    calendarId: number;
}

export class CalendarService{
    constructor(private calendarRepository: PrismaClient["calendar"]){}

    async createCalendar (DTO: CreateCalendarDTO){
        return await this.calendarRepository.create({
            data: {
                users: {
                    connect: [{ id: DTO.userId }]
                }
            },
            include: {
                users: true,
                events: true
            }
        })
    }

    async addUserCalendar (DTO: AddUserToCalendarDTO){
         return await this.calendarRepository.update({ 
            where:{ id: DTO.calendarId },
            data: {
                users: {
                    connect: [{ id: DTO.userId }]
                }
            },
            include: {
                users: true,
                events: true
            }
        });
    }

    async getCalendar (DTO: GetCalendarDTO){
        return await this.calendarRepository.findUnique({ 
            where:{ id: DTO.calendarId },
            include: {
                events: true,
                users: true
            }
        });
    }

    async fetchAllCalendar (){
        return await this.calendarRepository.findMany({
            include: {
                events: true,
                users: true
            }
        });
    }
}


