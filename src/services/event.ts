import { PrismaClient } from "@prisma/client"
import { CalendarService } from "./calendar"
import { UserService } from "./user"

// Are you there ?
type getEventDTO = {
    id: number
}
type AddEventDTO = {
    name: string;
    date: string;
    calendarId: number;
    creatorUserId: number;
}
type deleteEventDTO = {
    idEvent: number;
}
type updateEventDTO = {
    idEvent: number;
    name: string;
    date: string;
}

export class EventService {
    // Normalement on devrait faire un vrai repository mais j'ai la flemme
 
    constructor(
        private eventRepository: PrismaClient["event"],
        private userService: UserService
    ) {
    }

    // ✕ Shouldnt' allow creating two events on the same date 
   
    async addEvent(DTO: AddEventDTO) {
        const now = new Date(Date.now() - 1000 * 10);
        const createdEventDate = new Date(DTO.date);

        // Kewsss tu féééé
        if (new Date(DTO.date) < now) {
            throw "Oh no";
        } 
        
   //     // if .users.includes(userId) of calendar is not defined... throw err
   //STEP ONE fetch user by id 
   // STEP TWO verify calendar list user with event calendarId
        const user = await this.userService.getUserWithCalendar({id: DTO.creatorUserId});

        if (!user) {
            throw "lol"
        }

        const checkUserCalendar = user.Calendars.some(calendar => DTO.calendarId === calendar.id)
        if(!checkUserCalendar){
            throw 'user not registered in this calendar';
        }
 //   ✕ Shouldnt' allow creating an event without being an user of the calendar (49 ms)
        const eventsOnDate = await this.findEventsOnDate(createdEventDate, DTO.calendarId);
        if (eventsOnDate.length > 0) {
            //console.log("events on this date:", eventsOnDate)
            throw 'Two events on the same date';
        }

        const createEvent = await this.eventRepository.create({
            data: {
                name: DTO.name,
                date: DTO.date,
                Calendar: { connect: { id: DTO.calendarId } },
                creator: { connect: { id: DTO.creatorUserId } },
            }
        });

        return createEvent;
    }

    async findEventsOnDate(date, calendarId) {
        // SI date now + 30 min est inférieur  à la date de l'event. 
        const eventsAll = await this.fetchAllEventsForCalendar(calendarId);
        //const eventsAll = await this.fetchAllEvents();

        //console.log("events in calendar:", eventsAll)
        return eventsAll.filter(event => {
            // if (calendarId === event.calendarId)
            //     return false;

            const createdEventDate = new Date(date);
            const checkedEventDate = new Date(event.date);
            
            const event1after30Min = new Date(checkedEventDate);
            event1after30Min.setMinutes(event1after30Min.getMinutes() + 30)
            //👺🪄
            if( createdEventDate >= checkedEventDate && createdEventDate <= event1after30Min){
                return true;
            }
            
        });
    }
    
    //elle est là ta getEvent
    async getEvent(DTO: getEventDTO) {
 
        return await this.eventRepository.findUnique({
            where: {
                id: DTO.id,
        }})
        
    }

    async fetchAllEvents() {
        return await this.eventRepository.findMany();
    }

    async fetchAllEventsForCalendar(calendarId) {
        return await this.eventRepository.findMany({
            where: { calendarId: calendarId }
        });
    }

    async deleteEvent(DTO: deleteEventDTO){
        return await this.eventRepository.delete({
            where:{
                id:DTO.idEvent
            }
        })
    }
    async updateEvent(DTO: updateEventDTO){
        return await this.eventRepository.update({
            where:{
                id:DTO.idEvent
            },
            data:{
                name: DTO.name,
                date: DTO.date,
            }
        })
    }

    async rescheduleEvent(DTO: AddEventDTO) {
        return await this.addEvent(DTO); 
    }
}