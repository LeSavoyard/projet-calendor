import { PrismaClient } from '@prisma/client'

type AddUserDTO = {
    name: string;
}
type GetUserDTO = {
    id: number;
}
// t'as dit koi ?
// TKT
// wsh wsh kanapésh 

export class UserService {
 constructor( private userRepository: PrismaClient["user"] ) {
    }

    async add(DTO: AddUserDTO) {
        return await this.userRepository.create({
            data: {
                name: DTO.name,
            }
        });
    }
    async fetchUser() {
        return await this.userRepository.findMany();
    }
    async getUser(DTO: GetUserDTO) {
        return await this.userRepository.findUnique({
            where: {
                id: DTO.id
            }
        });
    }

    async getUserWithCalendar(DTO: GetUserDTO) {
        return await this.userRepository.findUnique({
            where: {
                id: DTO.id
            },
            include: {
                Calendars: true
            }
        });
    }
}

// export const fetchUser = async (res) => {

//     try {
//         const allUsers = await prisma.user.findMany()

//         res.status(200).json(allUsers!)
//     } catch (err) {
//         if (err) {
//             res.status(500).json(err)
//         }
//     }
// }

// export const add = async (user: AddUserDTO) => {
//     try {
//        const user = await prisma.user.create({
//             data: {
//                 name: req.body.name
//             }
//         })
//         res.status(200).json(user!)
//     } catch(e) {
//         res.json(e)
//     }
// }