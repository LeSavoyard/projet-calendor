import {Request, Response} from 'express';
import {CalendarService, CreateCalendarDTO, AddUserToCalendarDTO, GetCalendarDTO} from '../services/calendar';
import Joi from 'joi'
// wesh t'est ki en readonly ? 
// Mais c'est moi

export class CalendarController {

    constructor(private calendarService: CalendarService) { }

    async createCalendar(req: Request, res: Response) {

        try {
            const DTO: CreateCalendarDTO = {
                userId: req.body.userId
            };
            const schema = Joi.object({
                userId:Joi.number().required()
            });
            const validation = schema.validate(DTO);
            if (validation.error) {
                res.status(400).json({error:validation.error});
                return;
            }

            const calendar = await this.calendarService.createCalendar(DTO);

            res.status(201).json(calendar);
        } catch (e) {
            //console.log("createCalendar error", e);
            res.sendStatus(500);
        }
    }

    async addUserToCalendar(req: Request, res: Response) {
        try {
            const schema = Joi.object({
                userId: Joi.number().required(),
                calendarId: Joi.number().required()
            });

            const DTO: AddUserToCalendarDTO = {
                calendarId: Number(req.params.calendarId),
                userId: req.body.userId
            }

            const {value, error} = schema.validate(DTO);

            if (error) {
                res.status(400).json({ error: error.message });
                return;    
            }


            const calendar = await this.calendarService.addUserCalendar(value);
            res.status(200).json(calendar);
        } catch (e) {
            console.error(e);
            res.sendStatus(500);
        }
    }

    async getCalendar(req: Request, res: Response) {
        try {
            const DTO: GetCalendarDTO = {
                calendarId: Number(req.params.calendarId)
            };
            const schema = Joi.object({
                calendarId: Joi.number().required()
            });

            const {value, error} = schema.validate(DTO);

            if(error){
                res.sendStatus(400).json({error:error});
                return;
            }

            const calendar = await this.calendarService.getCalendar(DTO);
            res.status(200).json(calendar);
        } catch (e) {
            console.error(e);
            res.sendStatus(500);
        }
    }

    async fetchAll(req: Request, res: Response) {
        try {
            const calendar = await this.calendarService.fetchAllCalendar();
            res.status(200).json(calendar);
        } catch (e) {
            console.error(e);
            res.sendStatus(500);
        }
    }
}
