import {Request, Response} from 'express';
import { EventService } from 'src/services/event';

export class EventController {
    constructor( private eventService: EventService ) {}

    //  ✕ Shouldnt' allow creating two events on the same date (70 ms)
    //   ✕ Shouldnt' allow creating an event in the past (57 ms)
    //   ✕ Shouldnt' allow creating an event without being an user of the calendar (56 ms)

    async addEvent(req: Request, res: Response) {
        try {
            const DTO = {
                name: req.body.name,
                date: req.body.date,
                calendarId: req.body.calendarId,
                creatorUserId: req.body.creatorUserId
            }

            //faut faire les checks dans le eventService!!!
            const event = await this.eventService.addEvent(DTO);
            res.status(200).json(event);
            
        } catch(e) {
            res.sendStatus(500);
        }
    }
}
// Une méthode a disapear....
type AddEventDTO = {
    name: string;
}
