import { Request, Response } from "express";
import {  UserService } from '../services/user';

// const prisma = new PrismaClient();

//export const findAll = async (req: Request, res: Response) => fetchUser(res);

export class UserController {

    constructor( private userService: UserService ) {
    }

     async addUser(req: Request, res: Response) {
        try {
            const DTO = {
                name: req.body.name,
            }
            const event = await this.userService.add(DTO);
            res.status(200).json(event);
        } catch(e) {
            res.sendStatus(500);
        }
    }

      async findAll(req: Request, res: Response) {
        try {
            const users = await this.userService.fetchUser();
            res.status(200).json(users);
        } catch(e) {
            res.sendStatus(500);
        }
        
    }
    async findById(req: Request, res: Response ) {
        try {
            const DTO = {id: parseInt(req.params.id)}
            const userId = await this.userService.getUser(DTO); 
            res.status(200).json(userId)
        } catch(e) {
            res.sendStatus(500);
        }
    }
    // addUser = async (req: Request, res: Response) =>{
    

};



