# dev

```bash
npm install
npm run schemaUpdate

npm run dev #lance le serveur de dev

npm run test #lance les tests jest


npm run build
npm run start #lance le serveur buildé
```

# **Shared Agenda**

- Stack : SQLITE + TypeScript + Node / Express

* un user peut CRUD un calendrier
* un user peut ajouter un évènement à un calendrier
* un user peut ajouter des membres à un calendrier (calendrier ==== groupe)

# **DB relations**

- A User belongs to many calendar
- A Calendar belongs to many user
- A Calendar can have many event
- Many Events belongs to One Calendar
  En tant que membre d'un calendrier, je veux pouvoir créer un lien d'invitation a ce calendrier

# User stories

* En tant qu'utilisateur, je veux pouvoir créer un nouveau calendrier ![1613599334158.png](./img/1613599334158.png)
* En tant que créateur d'un calendrier, je veux pouvoir supprimer le calendrier![1613599280301.png](./img/1613599280301.png)
* En tant que membre d'un calendrier, je veux pouvoir créer un lien d'invitation à ce calendrier ![1613599243021.png](./img/1613599243021.png)
* En tant que membre d'un calendrier, je veux pouvoir CRUD des évenements sur un calendrier ![1613599353688.png](./img/1613599353688.png)
