import request from 'supertest';

import { app, prisma } from '../../src/server';

describe('Calendar route', () => {

    afterAll(async () => {
        prisma.$disconnect();
    })
    describe('FETCH ALL route', ()=>{
        describe('should reply with all calendar',()=>{
            it("should return 200 status code", async (done) => {

                const res = await request(app).get('/calendar');
        
                expect(res.status).toBe(200);
                expect(res.body).toBeInstanceOf(Array);
                expect(res.body).toContainEqual(
                        expect.objectContaining({
                            events: expect.any(Array),
                            users: expect.any(Array)
                        })
                )
                done();
            })
        })
    }),

    describe("GETONE route",()=>{
        describe("should reply with one calendar", ()=>{
            it("should return 200 status code", async (done) => {
                const res = await request(app).get('/calendar/1');

                expect(res.status).toBe(200);
                expect(res.body).toEqual({
                    id: expect.any(Number),
                    events: expect.any(Array),
                    users: expect.any(Array)
                })
                done();
            })
        })
    })

    describe("ADD USER CALENDAR route",()=>{
        describe("should add user to calendar", ()=>{
            it("should return 200 status code", async (done) => {

                const res = await request(app).put('/calendar/1/addUser').send({userId:1});
                
                expect(res.status).toBe(200);
                expect(res.body).toEqual({
                    id: expect.any(Number),
                    events: expect.any(Array),
                    users: expect.any(Array)
                })
                done();
            })
        })
        describe("should't add user to calendar", ()=>{
            it("should return 400 status code", async (done) => {
                const res = await request(app).put('/calendar/1/addUser');
                
                expect(res.status).toBe(400);
                done();
            })
        })
    })

    describe("CREATE CALENDAR route",()=>{
        describe("should create calendar", ()=>{
            it("should return 200 status code", async (done) => {
    
                const res = await request(app).post('/calendar').send({userId:1});
    
                expect(res.status).toBe(201);
                expect(res.body).toEqual({
                    id: expect.any(Number),
                    events: expect.any(Array),
                    users: expect.any(Array)
                })
                done();
            })
        })
        describe("should't create calendar", ()=>{
            it("should return 400 status code", async (done) => {
    
                const res = await request(app).post('/calendar');
    
                expect(res.status).toBe(400);
                done();
            })
        })
    })
})