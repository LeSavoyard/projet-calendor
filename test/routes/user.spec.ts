import request from 'supertest';
import { app, prisma } from '../../src/server';

describe('User route', () => {

    afterAll(async () => {
        prisma.$disconnect();
    })

    describe('list users', () => {
        it("should reply with the user list", async (done) => {
            
            const res = await request(app)
                .get('/users');
            
            expect(res.status).toBe(200);
            expect(res.body).toBeInstanceOf(Array);
            expect(res.body).toContainEqual(
                expect.objectContaining({
                    id: expect.any(Number),
                    name: 'john',
                })
            );

            done();
        })
    })

    describe('add user', () => {
        // OOOH ON EST OÙ LÀ
        it("should create a new user and return it", async (done) => {
            const res = await request(app)
                .post('/users')
                .send({name: 'john'})
            
            expect(res.status).toBe(200);
            expect(res.body).toEqual({
                id: expect.any(Number),
                name: 'john',
            });

            done();
        })
    })
})
