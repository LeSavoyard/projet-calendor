import { PrismaClient } from '@prisma/client'
import { CalendarService } from '../../src/services/calendar';
import { CalendarController } from '../../src/controllers/calendar';

let prisma: PrismaClient;
let calendarService: CalendarService;
let calendarController: CalendarController;

beforeAll(async () => {
    prisma = new PrismaClient();
    calendarService = new CalendarService(prisma.calendar);
    calendarController = new CalendarController(calendarService);
})

afterAll(async () => {
    await prisma.$disconnect();
})

const makeFakeExpressObjects = (body: any = {}, routeParams: any = {}) => {
  const req: any = {
    body,
    params: routeParams
  };

  const res: any = {};

  res.status = jest.fn(code => { return res });
  res.sendStatus = res.status;
  res.send = jest.fn(code => { return res });
  res.json = jest.fn(jsonObj => { return res });

  return [req, res];
}

const expectedUserObject = expect.objectContaining({
  id: expect.any(Number),
});
const expectedCalendarObject = expect.objectContaining({
  id: expect.any(Number),
  users: expect.arrayContaining([expectedUserObject]),
  events: expect.any(Array),
});


describe("createCalendar method", () => {
    it('should exist', async () => {
      expect(calendarController).toHaveProperty('createCalendar');
    })

    describe("successful creation", () => {
      const [req, res] = makeFakeExpressObjects({ userId: 1});


      beforeAll(async () => {
        await calendarController.createCalendar(req, res);
        console.log(req.body.userId);
      })

      it('should send a 201 status code', async () => {
        expect(res.status).toHaveBeenCalledWith(201);
      });
      it('should return the created calendar', async () => {
        expect(res.json).toHaveBeenCalledWith(expectedCalendarObject);
      });
    });

    describe("failing creation (unknown user)", () => {
      const [req, res] = makeFakeExpressObjects({ userId: -1});

      beforeAll(async () => {
       await calendarController.createCalendar(req, res);
      });

      it('should send a 500 status code', async () => {
        expect(res.sendStatus).toHaveBeenCalledWith(500);
      });

      it("should shouldn't send any json", async () => {
        expect(res.json).not.toHaveBeenCalled();
      });
    });

    describe("failing creation (no user given)", () => {
      const [req, res] = makeFakeExpressObjects({});

      beforeAll(async () => {
       await calendarController.createCalendar(req, res);
      });

      it('should validate and send a 400 status code', async () => {
        expect(res.sendStatus).toHaveBeenCalledWith(400);
      });
    });
    describe("failing creation (userId bullshit)", ()=>{
      
      const [req, res] = makeFakeExpressObjects({userId:"bullshit"});

      beforeAll(async () => {
       await calendarController.createCalendar(req, res);
      });

      it('should validate and send a 400 status code', async () => {
        expect(res.sendStatus).toHaveBeenCalledWith(400);
      });
    })
    
});

describe("fetchAll method", () => {

  it('should exist', () => {
    expect(calendarController).toHaveProperty('createCalendar');
  })

  const [req, res] = makeFakeExpressObjects();

  beforeAll(async () => {
    await calendarController.fetchAll(req, res);
  })

  it('should send a 200 status code', async () => {
    expect(res.status).toHaveBeenCalledWith(200);
  });
  it('should return a list of calendars', async () => {
    expect(res.json).toHaveBeenCalledWith(expect.arrayContaining([expectedCalendarObject]));
  });

});

describe('addUserToCalendar method',()=>{
  describe('failing creation (param bullshit)',()=>{
    const [req, res] = makeFakeExpressObjects({userId:"bullshit", calendarId:"bullshit"});

      beforeAll(async () => {
       await calendarController.createCalendar(req, res);
      });
    it('Should return 400 error status code',async()=>{

      expect(res.sendStatus).toHaveBeenCalledWith(400);
    })
  })
  describe('failing creation (param not set)',()=>{
    const [req, res] = makeFakeExpressObjects();

      beforeAll(async () => {
       await calendarController.createCalendar(req, res);
      });
    it('Should return 400 error status code',async()=>{

      expect(res.sendStatus).toHaveBeenCalledWith(400);
    })
  })

  describe('getCalendar method', ()=>{
    describe('failing fetchOne calendar (param not set)', ()=>{
      
      const [req, res] = makeFakeExpressObjects();
      beforeAll(async () => {
        await calendarController.getCalendar(req, res);
       });
      
       it('Should return 400 error status code', ()=>{
         expect(res.sendStatus).toHaveBeenCalledWith(400);
      })
    })
    describe('failing fetchOne calendar (param bullshit)', ()=>{
      
      const [req, res] = makeFakeExpressObjects({calendarId:'bullshit'});
      
      beforeAll(async () => {
        await calendarController.getCalendar(req, res);
       });
      
       it('Should return 400 error status code', ()=>{
         expect(res.sendStatus).toHaveBeenCalledWith(400);
      })
    })
    describe('successful fetch calendar', ()=>{
      
      const [req, res] = makeFakeExpressObjects({}, {calendarId:1});

      beforeAll(async () => {
        await calendarController.getCalendar(req, res);
       });
      
       it('Should return 200 status code', ()=>{
         expect(res.sendStatus).toHaveBeenCalledWith(200);
      })
    })
  })
})
