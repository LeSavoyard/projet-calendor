
import { PrismaClient } from '@prisma/client'
import { UserService } from '../../src/services/user';
import { EventService } from '../../src/services/event';

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Iterators_and_Generators
// always return dates that are `diff` ms appart
function* generateDatesAppart(start: Date, diff: number) {
    let date = new Date(start);
    while (true) {
        date = new Date(Number(date) + diff)
        //date.setMinutes(date.getMinutes() + diff)
        yield date;
    }

    return date;
}

describe("Event service", () => {
    const prisma = new PrismaClient();
    const userService = new UserService(prisma.user);
    const eventService = new EventService(prisma.event, userService);
    const dateGenerator = generateDatesAppart(new Date(), 1000 * 60 * 50);
    const getNextDate = () => dateGenerator.next().value;
    let calendarForTest;
    let nonMemberUserForTest;
    
    beforeAll(async () => {
        //create a calendar and some users for our tests
        calendarForTest = await prisma.calendar.create({
            data: {
                users: {
                    create: [
                        { name: "Bob" },
                        { name: "Bab" },
                        { name: "Boop" },
                        { name: "Bap" },
                    ]
                }
            },
            include: {
                users: true
            }
        });
        // 
        nonMemberUserForTest = await prisma.user.create({
            data: { name: "Bib" }
        })
    });

    afterAll(async () => {
        await prisma.$disconnect();
    })

    describe('addEvent method', () => {
        it ('Should create an event and return it', async () => {

            const result = await eventService.addEvent({
                name: "Grosse teuf à Simplon",
                date: getNextDate().toISOString(),
                creatorUserId: calendarForTest.users[0].id,
                calendarId: calendarForTest.id,
            });

            expect(result).toHaveProperty('name');
            
            expect(result).toMatchObject({
                id: expect.any(Number),
                date: expect.any(String),
                name: "Grosse teuf à Simplon",
            })
        })

        it ("Shouldnt' allow creating two events on the same date", async () => {
            const event = {
                name: "Grosse teuf à Simplon",
                date: getNextDate().toISOString(),
                creatorUserId: calendarForTest.users[0].id,
                calendarId: calendarForTest.id,
            };

            // pas sûr que ça marche celui-là
            //expect(eventService.addEvent(event)).resolves.not.toThrow();
            //expect(eventService.addEvent(event)).rejects.toThrow();
         
            // CRADE CRADE CRADE
            let threw = false;
            try {
                const event1 = await eventService.addEvent(event);

                const event2 = await eventService.addEvent({
                    ...event,
                    date: new Date(Number(new Date(event.date)) + 3000).toISOString()
                });
            } catch(e) {
                threw = true;
            }
            expect(threw).toBe(true);
        })

        it ("Shouldnt' allow creating an event in the past", async () => {
            const event = {
                name: "Grosse teuf à Simplon",
                date: new Date(Date.now() - 1000 * 60 * 60 * 24).toISOString(),
                creatorUserId: calendarForTest.users[0].id,
                calendarId: calendarForTest.id,
            };
            // Merciiiiiii :) \o/ 
            
            // CRADE CRADE CRADE
            let threw = false;
            try {
                await eventService.addEvent(event);
            } catch(e) {
                threw = true;
            }
            expect(threw).toBe(true);
        })

        it ("Shouldnt' allow creating an event without being an user of the calendar", async () => {
            const event = {
                name: "Grosse teuf à Simplon",
                date: getNextDate().toISOString(),
                creatorUserId: nonMemberUserForTest.id,
                calendarId: calendarForTest.id,
            };

            // CRADE CRADE CRADE
            let threw = "";
            try {
                await eventService.addEvent(event);
            } catch(e) {
                threw = e;
            }
            expect(threw).toMatch("user not registered in this calendar");
        })
    })
    //🐽 A touteeeee 🌞
    describe('getEvent method', () => {
        it ('Should return our event', async () => {
            const eventForTest = await eventService.addEvent({
                name: "Grosse teuf à Simplon",
                date: getNextDate().toISOString(),
                creatorUserId: calendarForTest.users[0].id,
                calendarId: calendarForTest.id,
            });
            //🐈🐈🐈🐈🐈🐈🐈🐈🧙🧙
            const result = await eventService.getEvent({ id: eventForTest.id });

            expect(eventForTest).toEqual(result);
        })

        it ('Should return null on unknown events', async () => {
            const result = await eventService.getEvent({ id: -1 });

            expect(result).toBeNull();
        })
    })

    describe('updateEvent method', () => {
        
        let eventForTest;
        
        beforeAll(async () => {
            eventForTest = await eventService.addEvent({
                name: "Grosse teuf à Simplon",
                date: getNextDate().toISOString(),
                creatorUserId: calendarForTest.users[0].id,
                calendarId: calendarForTest.id,
            });
        })
        it('should update an event then return it', async () => {

            const update = await eventService.updateEvent({ idEvent : eventForTest.id, name : 'le nom a été modifié', date : eventForTest.date });
            const result = await eventService.getEvent({id : eventForTest.id });
            expect(update).toEqual(result);
        })

        expect(eventService).toHaveProperty('updateEvent')
    })
// Ma qué l'id ? 
    describe('deleteEvent method', () => {
        let eventForTest;
        beforeAll(async () => {
            eventForTest = await eventService.addEvent({
                name: "Grosse teuf à Simplon",
                date: getNextDate().toISOString(),
                creatorUserId: calendarForTest.users[0].id,
                calendarId: calendarForTest.id,
            });
        })
        it('should delete an event', async () => {
            const deleting = await eventService.deleteEvent({idEvent: eventForTest.id});
            expect(eventForTest).toEqual(deleting);
        })
    })
// Do Not erase this test please... 
    describe('rescheduleEvent method', () => {
        it('should be defined', async () => {
            expect(eventService).toHaveProperty('rescheduleEvent')
        })
    })
  });
