import { PrismaClient } from '@prisma/client'
import { UserService } from '../../src/services/user';


describe("User service", () => {
    const prisma = new PrismaClient();
    const userService = new UserService(prisma.user);

    afterAll(async () => {
        await prisma.$disconnect();
    })

    describe('add method', () => {
        it ('Should create an user and return it', async (done) => {
            const result = await userService.add({ name: "Bob" });
            expect(result).toHaveProperty('name');

            // expect(result).toMatchObject({
            //     id: expect.any(Number),
            //     name: "Bob"
            // })

            done();
        })
    })

    describe('getUser method', () => {
        it ('Should return an user', async () => {
            await userService.add({ name: "Bob" });

            const result = await userService.getUser({ id: 1});
            
            //expect(result).toMatchObject({id: Number, name: String});
            //expect(result).toMatchObject({id: 1, name: "Bob"});
            expect(result).toMatchObject({id: expect.any(Number), name: "Bob"})
        })
    })
//    { id: expect.any(Number), name: "Bob"},
               // { id: expect.any(Number), name: "Bob2"},
    describe('fetchUser method', () => {
        expect(userService).toHaveProperty('fetchUser');
        
        it('Should return a list of all users', async () => {
            await userService.add({ name: "Bob" });
            await userService.add({ name: "Bob2" });

            const userList = await userService.fetchUser();

            expect(userList).toBeInstanceOf(Array);
            expect(userList).toContainEqual(
                   expect.objectContaining({
                    id: expect.any(Number),
                    name: 'Bob',
                })
            );
            expect(userList).toContainEqual(
                   expect.objectContaining({
                    id: expect.any(Number),
                    name: 'Bob2',
                })
            );
        })
    })
});
