import { PrismaClient } from '@prisma/client'
import { CalendarService } from '../../src/services/calendar';


describe("Calendar service", () => {
    const prisma = new PrismaClient();
    const calendarService = new CalendarService(prisma.calendar);

    afterAll(async () => {
        await prisma.$disconnect();
    })

    describe('addUserToCalendar method', () => {
        expect(calendarService).toHaveProperty('addUserCalendar');

        it('should add the user to the calendar', async() => {
            const inputDTO = {
                calendarId:1,
                userId:1
            }

            const result = await calendarService.addUserCalendar(inputDTO);

            expect(result.id).toBeDefined();

            expect(result).toHaveProperty('id');
            expect(result).toHaveProperty('users');
            expect(result).toHaveProperty('events');
            
            expect(result).toMatchObject({
                id: 1,
                users: expect.arrayContaining([
                    expect.objectContaining({
                        id: inputDTO.userId
                    })
                ]),
                events: expect.arrayContaining([
                    expect.objectContaining({
                        id: inputDTO.userId
                    })
                ]),
            })
            
        })
    })

    describe('fetchAllCalendar method', () => {
        it('should return a calendar list', async () => {
            const res = await calendarService.fetchAllCalendar();

            expect(res).toBeInstanceOf(Array);
            expect(res.length>0).toBe(true)
        })
    })

    describe('createCalendar method', () => {
        expect(calendarService).toHaveProperty('createCalendar');
 
        //TODO
    });

    describe('getCalendar method', () => {
        expect(calendarService).toHaveProperty('getCalendar');
        it('Should return one calendar', async ()=>{
            
            const DTO = {
                calendarId:1
            };

            const res = await calendarService.getCalendar(DTO);
//             creator User
//              name   String
//              date   String
            
            expect(DTO).toHaveProperty('calendarId');
            expect(res).toMatchObject({
                id: expect.any(Number),
                users: expect.arrayContaining([
                    expect.objectContaining({
                        id: expect.any(Number),
                        name: expect.any(String),
                    }),
                ]),
                events: expect.arrayContaining([
                    expect.objectContaining({
                        id:expect.any(Number),
                        calendarId: expect.any(Number),
                        date: expect.any(String),
                        userId: expect.any(Number)
                    })
                ])
            });
        })
    })
})
